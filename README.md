# Install prometheus/grafana on fedora 27 with ansible

I made a small Ansible playbook for installing prometheus and grafana on fedora 27 origin installation.
Clone the ansible playbook from bitbucket. (https://bitbucket.org/arozendaal/blogfromandre/src)

```sh
[user@system tmp]$ git clone https://bitbucket.org/arozendaal/blogfromandre/
Cloning into 'blogfromandre'...
remote: Counting objects: 12, done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 12 (delta 3), reused 0 (delta 0)
Unpacking objects: 100% (12/12), done.
```
Running the install/run playbook.
```sh
[user@system tmp]$ cd blogfromandre/
[user@system blogfromandre]$ ansible-playbook setup_prometheus_grafana.yml
```
For installing openshift origin and prometheus you can run the two playbooks.

```sh
[user@system blogfromandre]$ ansible-playbook start_oc.yml setup_prometheus_grafana.yml
```