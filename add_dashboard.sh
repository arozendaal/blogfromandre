#!/bin/bash

for datasource in `ls grafana/ds-*` 
do
  export datasource_json=$(cat $datasource|tr "\n" " ")
  dashboard_json='{'"$datasource_json"'}'
  curl http://admin:admin@grafana-prometheus.127.0.0.1.nip.io/api/datasources -X POST -H 'Content-Type: application/json;charset=UTF8' --data-binary ''"$datasource_json"''
done

export dashboard_json=$(cat grafana/dashboard.json|tr "\n" " ")
dashboard_json='{"dashboard":'"$dashboard_json"'}'
curl http://admin:admin@grafana-prometheus.127.0.0.1.nip.io/api/dashboards/db -X POST -H 'Content-Type: application/json;charset=UTF8' --data-binary ''"$dashboard_json"''

